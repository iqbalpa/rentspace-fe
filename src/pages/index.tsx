import { LandingModule } from '@modules'
import { NextPage } from 'next'

const Home: NextPage = () => <LandingModule />

export default Home
