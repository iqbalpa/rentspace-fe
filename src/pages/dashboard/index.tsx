import React from 'react'
import type { NextPage } from 'next'
import { AdminDashboardModule } from '@modules/DashboardModule/Admin'
import { ProviderDashboardModule } from '@modules/DashboardModule/Provider'

const user = 'ADMIN' // dummy

const Dashboard: NextPage = () => (
  <>
    {user == 'ADMIN' ? <AdminDashboardModule /> : <ProviderDashboardModule />}
  </>
)

export default Dashboard
