import React from 'react'
import type { NextPage } from 'next'
import { SpacesModule } from '@modules/SpacesModule/Catalogue'

const SpacesDetail: NextPage = () => <SpacesModule />

export default SpacesDetail
