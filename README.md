This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

1. After cloning the repository, install all dependencies:

   ```bash
   yarn
   ```

2. Run the development server:

   ```bash
   yarn dev
   ```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.
- [React DaisyUI](https://react.daisyui.com/?path=/story/welcome--page)
